# README #

Welcome to the STTG3 Project. 

### What is this repository for? ###

This is a web app for the group project in the Software Technologies and Techniques course (ELEN7046) at the University of the Witwatersrand
(The ELEN7046 subject is part of the M Eng programme in Software Engineering)


### How do I get set up? ###

For comprehensive setup instructions including how to configure git please visit the [Wiki](https://bitbucket.org/snyman_t/sttg3webapp/wiki/home)


### Who do I talk to? ###

The following team members are contributing to the project:
Thinus Snyman
Shameer Kika
Ushanta Mooloo
Mohammed Kazi