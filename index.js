
var express = require('express');

var app = express();
var ejs = require('ejs');

app.use(express.static('public'));
app.use(express.static('local'));

app.set('view engine', 'html');
app.engine('html', ejs.renderFile);

var bodyParser = require('body-parser');
app.use(bodyParser.json()); // support json encoded bodies
app.use(bodyParser.urlencoded({ extended: true })); // support encoded bodies

var cookieSession = require('cookie-session')

app.use(cookieSession({
  name: 'server-session-cookie-id',
  secret: 'my express secret',
  saveUninitialized: true,
  resave: true
}));

app.set('views',
[__dirname + '/views',
__dirname + '/views/indicator',
__dirname + '/views/country',
__dirname + '/views/map',
__dirname + '/views/hierarchy'
]);


function processRequest(view,req){
	//--- Set the defaults ---------
	if(typeof req.session.countries == 'undefined'){
		req.session.countries = ['abw','ado','afg','ago','alb'];
	}

	if(typeof req.session.fromYear == 'undefined'){
		req.session.fromYear = '1990'
	}

	if(typeof req.session.toYear == 'undefined'){
		req.session.toYear = '2000'
	}

	if(typeof req.session.indicators == 'undefined'){
		req.session.indicators = 'en.atm.co2e.kt'
	}	

	

	var stylesJsConfForView = require('./config/' + view + '.json');
	var globalVariables = require('./config/globalVariables.json');


	for(var sessionVariable in req.session){
	 	if(typeof req.body[sessionVariable] != 'undefined'){
	 		req.session[sessionVariable] = req.body[sessionVariable];
	 	}
	 }


	 req.query.scripts = stylesJsConfForView.scripts;
	 req.query.styles = stylesJsConfForView.styles;
	 req.query.session = req.session;

	 req.query.globalVariables = new Object();
	 req.query.globalVariables = globalVariables;
	 
	 req.query.session.view = view;
	 req.query.multipleCountries = true;

	 return req;
}

app.get('/', function (req, res) {
	 req = processRequest('hierarchy',req);
 	 res.render('hierarchy',req.query);
});

app.all('/country', function (req, res) {
	 req = processRequest('country',req);
	 req.query.multipleCountries = false;
 	 res.render('country',req.query);
});

app.all('/indicator', function (req, res) {

	req = processRequest('indicator',req);
 	res.render('indicator',req.query);
});

app.get('/map', function (req, res) {

	req = processRequest('map',req);
 	res.render('map',req.query);
});

app.get('/hierarchy', function (req, res) {

	req = processRequest('hierarchy',req);
 	res.render('hierarchy',req.query);
});

app.post('/updatesession', function (req, res) {
	
	for(var postedVariable in req.body){
		req.session[postedVariable] = req.body[postedVariable];
	}

 	res.send('Session updated');
});

app.listen(process.env.PORT || 3000, function () {
  console.log('App listening on port 3000!');
});
