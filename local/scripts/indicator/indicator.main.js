

    require(["scripts/indicator/indicator.elastic"], function (elastic) {

      elastic.populateIndicatorDropdown(); 
      elastic.plotCharts(window.session.countries.split(','),window.session.indicators,window.session.fromYear,window.session.toYear);

  
      //--- Handle the controls in the dashboard-----
      $(document).ready(function(){

          //--- Indicator Dropdown----
          $("#indicatorSelect").change(function(){
                
                //-------- Get the variables that were submitted
                var indicatorsSelected = $('#indicatorSelect').val();

                // ------------ Update the session on the server side
              var formData = 'indicators=' + indicatorsSelected;

                $.post(  
                  '/updatesession',  
                  formData,  
                  function(responseText){  
                    //window.location.reload();
                  },  
                  "html"  
                );
                
                //-------- Update the session on the client side
                window.session.indicators = indicatorsSelected;

                //------------ Plot the charts
                elastic.plotCharts(window.session.countries.split(','),indicatorsSelected,window.session.fromYear,window.session.toYear);
            })

            //---- The main form------
            $("#mainForm").submit(function(event){

              //-------- Get the variables that were submitted
              var yearsSelected = $("#timeslider>input").slider('getValue');
              var fromYear = yearsSelected[0];
              var toYear = yearsSelected[1];

              var countries = $('#countrySelect').val().join(",");

              
              // ------------ Update the session on the server side
              var formData = 'fromYear=' + fromYear + '&toYear=' + toYear + '&countries='+countries;

                $.post(  
                  '/updatesession',  
                  formData,  
                  function(responseText){  

                  },  
                  "html"  
                );

              // ----------- Update the session on the client side
                window.session.countries = countries;
                window.session.fromYear = fromYear;
                window.session.toYear = toYear;

              //------------ Plot the charts
                elastic.plotCharts(countries.split(','),window.session.indicators,fromYear,toYear);
              
              //------------Do not reload the entire page (This is a Single Page Web App) 
                event.preventDefault();
            })
            
          })


        

    });