
define(['elasticsearch/elasticsearch'], function (elasticsearch) {
"use strict";
    

    var client = new elasticsearch.Client(
        {
         host: window.globalVariables.elasticsearch_URL
        }
        );
    

    function populateIndicatorDropdown(){

      client.search({
          //index: 'small-worldbankdata-2016',
          index: window.globalVariables.elasticsearch_index,
          size: 10,
          body: {
              // Begin query.
      
              "aggs" : 
              {
                  "indicators" : {
                               "terms" : {
                                "size" : 100,
                                "field" : "Indicator Code"
                                         }
                  }
              }


              // End query.
          }
      }).then(function (response) {

          var indicators = new Array();

          for(var i = 0; i < response.aggregations.indicators.buckets.length; i++){
            indicators.push(response.aggregations.indicators.buckets[i].key);
          }


        //----------------Indicator Select-----------------------

          var indicatorsSelected = window.session.indicators.split(',');

          for(var i = 0; i < indicators.length; i++){
              
              var selected = '';
              if(indicatorsSelected.indexOf(indicators[i]) != -1){
                selected = 'selected';
              }

              $('#indicatorSelect').append('<option value="'+indicators[i]+ '" '+selected+' >'+indicators[i]+'</option>');
          }

          $('#indicatorSelect').multiselect();


    });
    	
    }



	function plotCharts(countries,indicator,fromYear,toYear){

	    client.search({
          index: window.globalVariables.elasticsearch_index,
          body: {
                            // Begin query.
                 "query": {
                      "filtered": {
                          "filter": {
                              "bool" : {
                                "must" : [ 
                                    //{ "term" : {"Indicator Code": "en.atm.co2e.kt"}},
                                    { "term" : {"Indicator Code": indicator}},
                                    //{ "terms" : {"Country Code": ["alb","abw","ado","afg","ago"]}},
                                    { "terms" : {"Country Code": countries}},
                                    {"range" : {"Year" : { "from" : fromYear, "to" : toYear }}} 
                                    ]
                              }
                          }
                      }
                  },
                  "aggs":
                  {
                  "countrysum" : {
                               "terms" : {
                                          "size": 300,
                                          "field" : "Country Name",
                                          "order" : { "valuesum" : "desc"}
                                         },
                                 "aggs": {
                                      "valuesum": {
                                                   "sum": {"field": "Value"}
                                                       }
                                        } 
                  },
                "countryyearsum" : {
                               "terms" : {
                                "size": 300,
                                "field" : "Country Name"

                                         },
                               "aggs": {
                                      "yearsum": {
                                                 "terms" : {
                                                  "size": 100,
                                                  "field" : "Year"

                                                       },
                                                  "aggs": {
                                                        "valuesum": {
                                                                     "sum": {"field": "Value"}
                                                                         }
                                                        }     
                                                   }
                                        } 
                  }
                 
                }
              // End query.
          }
	    }).then(function (response) {

	    	console.log(response);
        console.log('dasd');
//            window.session.indicatorsSelectedNames = new Array();
//            window.session.indicatorsSelectedNames = [response.hits.hits[0]._source["Indicator Name"]];

            $('#indicatorPageHead').html(response.hits.hits[0]._source["Indicator Name"]);

          	plotPieChart(response);

          	plotLineChart(response);

          	plotBarChart(response);


		});

	}


	function plotPieChart(response){

        		var chartData = new Array();

            //-----Transform the response data to Chart Data-----
            for(var i = 0; i < response.aggregations.countrysum.buckets.length; i++){
                  var label =  response.aggregations.countrysum.buckets[i].key;
                  var value = response.aggregations.countrysum.buckets[i].valuesum.value;

                  chartData[i] = new Object();
            
                  chartData[i].label = label;
                  chartData[i].value = value;

            }
          	

           //---- plot the chart-----
           nv.addGraph(function() {
          
            var chart = nv.models.pieChart()
                  .x(function(d) { return d.label })
                  .y(function(d) { return d.value })
                  .showLegend(false);
                  //.showLabels(true);

              d3.select("#piechart")
                  .datum(chartData)
                  .transition().duration(350)
                  .call(chart);


              //----- Click handler-------    
              d3.selectAll(".nv-slice").on("click", function () {
    			     //alert(this.__data__.data.label + " clicked!!");
				      });


              return chart;
          });
	}

	function plotLineChart(response){

		var chartData = new Array();

    //-----Transform the response data to Chart Data-----
    for(var i = 0; i < response.aggregations.countryyearsum.buckets.length; i++){
        chartData[i] = new Object();
        chartData[i].key = response.aggregations.countryyearsum.buckets[i].key;

        chartData[i].values = new Array();

        for(var j = 0; j < response.aggregations.countryyearsum.buckets[i].yearsum.buckets.length; j++){
            chartData[i].values[j] = new Object();
            chartData[i].values[j]['x'] = response.aggregations.countryyearsum.buckets[i].yearsum.buckets[j].key;
            chartData[i].values[j]['y'] = response.aggregations.countryyearsum.buckets[i].yearsum.buckets[j].valuesum.value;
        }

    }	

    //-----Plot the chart----------
		 nv.addGraph(function() {
  		var chart = nv.models.lineChart()
                .margin({left: 100})  //Adjust chart margins to give the x-axis some breathing room.
                .useInteractiveGuideline(true)  //We want nice looking tooltips and a guideline!
 //               .transitionDuration(350)  //how fast the lines should transition
                .showLegend(false)       //Don't show the legend
                .showYAxis(true)        //Show the y-axis
                .showXAxis(true) ;       //Show the x-axis

		  chart.xAxis     //Chart x-axis settings
		      .axisLabel('Year')
		      //.tickFormat(d3.format(',r'));
          //.showLegend(true);


		  chart.yAxis     //Chart y-axis settings
		      //.axisLabel(indicator)
		      //.tickFormat(d3.format('.02f'));


		  d3.select('#linechart')    //Select the <svg> element you want to render the chart in.   
		      .datum(chartData)         //Populate the <svg> element with chart data...
		      .call(chart);          //Finally, render the chart!

		  //Update the chart when window resizes.
		  //nv.utils.windowResize(function() { chart.update() });

      // --- Click Handler----
       d3.selectAll(".nv-line").on("click", function () {
         // alert(" clicked!!");
        }); 

		  return chart;
		});

	}

	function plotBarChart(response){

		var chartData = new Array();

    //-----Transform the response data to Chart Data-----

		chartData[0] = new Object();
		chartData[0].key = 'Indicator';
		chartData[0].values = new Array();				
    
    for(var i = 0; i < response.aggregations.countrysum.buckets.length; i++){
        var label = response.aggregations.countrysum.buckets[i].key;
        var value = response.aggregations.countrysum.buckets[i].valuesum.value;

        chartData[0].values[i] = new Object;
        chartData[0].values[i].label = label;
        chartData[0].values[i].value = value;
    }


    //-----Plot the chart----------
		nv.addGraph(function() {
  		var chart = nv.models.discreteBarChart()
      .x(function(d) { return d.label })    //Specify the data accessors.
      .y(function(d) { return d.value })
      .staggerLabels(true)    //Too many bars and not enough room? Try staggering labels.
//      .tooltips(false)        //Don't show tooltips
      .showValues(false)       //...instead, show the bar value right on top of each bar.
 //     .transitionDuration(350)
      .rotateLabels(-45)
      .height(320)
      ;

    d3.select('#barchart')
      .datum(chartData)
      .call(chart);

      // --- Click Handler----
      d3.selectAll(".nv-bar").on("click", function () {
      //alert(this.__data__.label + " clicked!!");
	   });

  nv.utils.windowResize(chart.update);

  return chart;
});



	}

  // --- Declare the publicly exposed functions of this module
	return {
		plotCharts : plotCharts,
    populateIndicatorDropdown : populateIndicatorDropdown
	}




});
