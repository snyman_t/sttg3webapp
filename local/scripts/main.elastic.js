define(['elasticsearch/elasticsearch'], function (elasticsearch) {
"use strict";
  function toTitleCase(str) {
    str = str.toLowerCase().split(' ');
    for (var i = 0; i < str.length; i++) {
      str[i] = str[i].charAt(0).toUpperCase() + str[i].slice(1); 
    }
    return str.join(' ');
  }

  var client = new elasticsearch.Client({ host: window.globalVariables.elasticsearch_URL });

  function populateCountriesDropdown(){
  	client.search({
        index: window.globalVariables.elasticsearch_index,
        size: 0,
        body: 
        {
			    "aggs" : 
  				{
  				    "countries" : 
              {
               "terms" : 
               {
               	"size" : 248,
               	"field" : "Country Code"
               },
                "aggregations" : { 
                    "Name": {
                        "terms": {"field": "Country Name",
                                  "order": {  "_count" : "asc"  }
                        }
                    }
                }
  						}
  				}
        }
    }).then(function (response) {
     // console.log(response);
        	var countries = new Array();

        	for(var i = 0; i < response.aggregations.countries.buckets.length; i++){
        		countries.push({Code: response.aggregations.countries.buckets[i].key, Name: response.aggregations.countries.buckets[i].Name.buckets[0].key});
        	}
          //console.log(countries);
          //countries.sort((a, b) => a - b);
        	//-----------------Country Select---------------------------
          var countriesSelected = window.session.countries.split(',');

          for(var i = 0; i < countries.length; i++){
              
              var selected = '';
              if(countriesSelected.indexOf(countries[i].Code) != -1){
                selected = 'selected';
              }

              $('#countrySelect').append('<option value="'+countries[i].Code+ '" '+selected+' >'+ toTitleCase(countries[i].Name) +'</option>');
          }
          
          $('#countrySelect').multiselect({
              enableFiltering: true,
              includeSelectAllOption: true
          });


          // By default all countries are selected and the country selection disabled in the map dashboard 
          if(window.session.view == 'map'){
          $('#countrySelect').multiselect('selectAll', false);
          $('#countrySelect').multiselect('updateButtonText');
          $('#countrySelect').multiselect('disable');
          }

			});
  }

  return {
	 populateCountriesDropdown : populateCountriesDropdown
  }
});
