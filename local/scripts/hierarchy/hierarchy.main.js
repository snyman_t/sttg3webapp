/**
 * ********** Change History **********
 * Date - User - Comment
 * 2016/06/04 - Shameer - Initial Version
 */
require(["scripts/hierarchy/hierarchy.elastic"], function (elasticJs) {
    elasticJs.setupHierarchyIndicators();
    elasticJs.refreshScreen()

    /* ====================================================================================== */

    /* ****************************************************************** */
    // This method is called when document initially loads
    /* ****************************************************************** */
    $(document).ready(function() {
        elasticJs.refreshScreen();
    })

    /* ****************************************************************** */
    // This method is called when hierarchy Indicator selection changes
    /* ****************************************************************** */
    $("#hierarchyIndicatorSelect").change(function() {
        // Get the variables that were submitted
        var userSelected = $('#hierarchyIndicatorSelect').val();
        // Update session on the server side with user selection
        var formData = 'hierarchySelected=' + userSelected;

        $.post('/updatesession', formData, function(responseText) {
        }, "html");

        window.session.hierarchySelected = userSelected;

        elasticJs.refreshScreen();
    })


    /* ****************************************************************** */
    // This method is called when form is submitted
    /* ****************************************************************** */
    $("#mainForm").submit(function(event) {

        //Determine the year from/to selected
        var yearsSelected = $("#timeslider>input").slider('getValue');
        var fromYear = yearsSelected[0];
        var toYear = yearsSelected[1];

        //Syncronize year from/to selection with session
        var formData = 'fromYear=' + fromYear + '&toYear=' + toYear;
        $.post('/updatesession', formData, function(responseText) {
        }, "html");
        window.session.fromYear = fromYear;
        window.session.toYear = toYear;

        //request refresh of screen
        elasticJs.refreshScreen();
        //Do not handle submit event further
        event.preventDefault();
    })
});