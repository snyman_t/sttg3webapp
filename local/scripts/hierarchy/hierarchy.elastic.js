/**
 * ********** Change History **********
 * Date - User - Comment
 * 2016/06/04 - Shameer - Initial Version
 */

define([
    'd3/d3.min',
    'elasticsearch/elasticsearch'],
      function (d3, elasticsearch) {
          "use strict";
          var client = new elasticsearch.Client({ host: window.globalVariables.elasticsearch_URL});

          /* ====================================================================================== */

          /* ****************************************************************** */
          //  This method manually initializes the hierarchy indicator selection comboBox
          //  NOTE : the list of hierarchy indicators available for selection is "hardcoded"
          //  for future scope, the list can be dynamically looked up from database, depending if sufficient hierarchy structure .
          /* ****************************************************************** */
          function setupHierarchyIndicators() {

              //Manually initialize the data for selection in the ComboBox
              var selectionArray = new Array();
              selectionArray.push({value: "EN", description: "C02/Greenhouse gas Hierarchy"});
              selectionArray.push({value: "NY", description: "GDP Hierarchy"});
              selectionArray.push({value: "ER", description: "Fisheries/AquaFarming Hierarchy"});

              //Determine which option was selected from session
              var sessionHierarchySelected = window.session.hierarchySelected;
              if (sessionHierarchySelected == null || sessionHierarchySelected == undefined) {
                  sessionHierarchySelected = "EN";
                  window.session.hierarchySelected = sessionHierarchySelected;
              }
              // setup the Combo Box
              for (var i = 0; i < selectionArray.length; i++) {
                  var selected = '';
                  if (selectionArray[i].value == sessionHierarchySelected) {
                      selected = 'selected';
                  }

                  $('#hierarchyIndicatorSelect').append('<option value="' + selectionArray[i].value + '" '
                          + selected + ' >' + selectionArray[i].description + '</option>');
              }

              $('#hierarchyIndicatorSelect').multiselect();
          }

          /* ********************************************************************************** */
//           This method is called reload data from backend and render the screen.
//           It fetches data from ElasticSearch, and plots the relevant graphs on the screen
          /* ********************************************************************************** */

          function refreshScreen() {
              var hierarchySelected = window.session.hierarchySelected;

              console.log("hierarchySelected" + hierarchySelected);

              client.search({
                  //TODO index: window.globalVariables.elasticsearch_index,
                  index: 'world_bank_data_index',
                  size: 0,
                  body: {
                      // Begin query.
                      "query": {
                          "bool": {
                              "must": {
                                  "prefix": {
                                      //"Comment : in the current database the following Indicator Codes root nodes
                                      // have good depth of hierarchies ": "ER", "EN", "NY"
                                      "Indicator Code": hierarchySelected
                                  }
                              },
                              "must_not": {
                                  "prefix": {
                                      "Value": 0
                                  }
                              }
                          }
                      },
                      "aggs": {
                          "indicator_group": {
                              "terms": {
                                  "field": "Indicator Code"
                              },
                              "aggs": {
                                  "indicator_name_group": {
                                      "terms": {
                                          "field": "Indicator Name"
                                      },
                                      "aggs": {
                                          "total_count": {
                                              "value_count": {
                                                  "field": "Value"
                                              }
                                          }
                                      }
                                  }
                              }
                          }
                      }// End query.
                  }
              }).then(function (response) {
                  console.log(response);

                  var responseDataArray = extractArrayFromElasticResponseData(response);

                  var chartData = extractGraphFormatDataFromElasticResponseArray(responseDataArray);

                  // Plot the Sunburst hierarchy Graph - needs data to be enclosed with "[....]"
                  plotSunburstGraph(JSON.parse("[" + JSON.stringify(chartData) + "]"));

                  // Plot the Tree hierarchy Graph
                  plotTreeGraph(chartData);

              });
          }


          /* ********************************************************************************************* */
//           This method is used to extract an array of aggregation items from ElasticSearch response
          /* ********************************************************************************************* */
          function extractArrayFromElasticResponseData(response) {
              var resultArray = [];

              var chartData = new Array();

              for (var i = 0; i <= response.aggregations.indicator_group.buckets.length - 1; i++) {

                  //Extract Indicator Code
                  var aIndicatorObject = response.aggregations.indicator_group.buckets[i];
                  var elementLabel = aIndicatorObject.key;

                  //Extract Indicator Description - will always have only 1 bucket
                  var aIndicatorDescriptionObject = aIndicatorObject.indicator_name_group.buckets[0];
                  var elementDescription = aIndicatorDescriptionObject.key;

                  //Extract Total Count
                  var elementValue = aIndicatorDescriptionObject.total_count.value;

                  console.log('elementLabel=' + elementLabel
                          + ' ;elementDescription=' + elementDescription
                          + ' ;elementValue=' + elementValue);
                  var elementArr = [];
                  elementArr.push(elementLabel);
                  elementArr.push(elementValue);
                  elementArr.push(elementDescription);
                  resultArray.push(elementArr);
              }

              console.log('resultArray.length=' + resultArray.length);
              return resultArray;
          }


          /* ********************************************************************************************* */
//           This method is used to construct the data required to render the graph,
//           it uses the array extracted from method "extractArrayFromResponseData"
          /* ********************************************************************************************* */
          function extractGraphFormatDataFromElasticResponseArray(someStringArray) {
              console.log("in extractChartDataFromElasticResponse");

              //NOTE : Initialize root
              var rootnode = {name:'null', children:[], size:0};
              for (var i1 = 0; i1 < someStringArray.length; i1++) {
                  extractJsonDataForSingleRecord(rootnode, someStringArray[i1][0], someStringArray[i1][1], someStringArray[i1][2]);
              }

              console.log("TESTING=" + JSON.stringify(rootnode));

              return rootnode;
          }

          /* ********************************************************************************************* */
//           This method is used to construct a JSon format of the individual graph items
          /* ********************************************************************************************* */
          function extractJsonDataForSingleRecord(rootnode, indicatorString, indicatorValue, indicatorDescription) {
              console.log("in extractJsonDataForSingleRecord()");

              //Using Example someString = "A1.B1.C1.D1.E1";
              var splitArray = indicatorString.split(".");
              var arrayLength = splitArray.length;
              console.log("arrayLength=" + arrayLength);

              //Store Level0	eg A1
              if (arrayLength > 0) {
                  rootnode.name = splitArray[0]; //AA
              }
              //Store Level1 	eg A1.B2
              if (arrayLength > 1) {
                  var foundFlag = false;
                  var nodeValue = splitArray[0] + "." + splitArray[1];
                  //search if existing level1 node matches
                  for (var i1 = 0; i1 < rootnode.children.length; i1++) {
                      if (rootnode.children[i1].name == nodeValue) {
                          foundFlag = true;
                      }
                  }
                  //if not found, store new level1 node
                  if (!foundFlag) {
                      if (arrayLength == 2) {
                          console.log("HERE1");
                          rootnode.children.push({name:nodeValue, children:[], size:indicatorValue, description:indicatorDescription});
                      } else {
                          rootnode.children.push({name:nodeValue, children:[], size:0});
                      }
                  }
              }
              //Store Level2 eg A1.B2.C1
              if (arrayLength > 2) {
                  // find correct level1 node
                  for (var i1 = 0; i1 < rootnode.children.length; i1++) {
                      var nodeValue = splitArray[0] + "." + splitArray[1];
                      if (rootnode.children[i1].name == nodeValue) {
                          var foundFlag = false;
                          nodeValue = nodeValue + "." + splitArray[2];
                          //search if existing level2 node matches
                          for (var i2 = 0; i2 < rootnode.children[i1].children.length; i2++) {
                              if (rootnode.children[i1].children[i2].name == nodeValue) {
                                  foundFlag = true;
                              }
                          }
                          //if not found, store new level2 node
                          if (!foundFlag) {
                              if (arrayLength == 3) {
                                  console.log("HERE2");
                                  rootnode.children[i1].children.push({name:nodeValue, children:[], size:indicatorValue, description:indicatorDescription});
                              } else {
                                  rootnode.children[i1].children.push({name:nodeValue, children:[], size:0});
                              }
                          }
                      }
                  }
              }
              //Store Level3 eg A1.B2.C1.D1
              if (arrayLength > 3) {
                  // find correct level1 node
                  for (var i1 = 0; i1 < rootnode.children.length; i1++) {
                      var nodeValue = splitArray[0] + "." + splitArray[1];
                      if (rootnode.children[i1].name == nodeValue) {
                          // find correct level2 node
                          nodeValue = nodeValue + "." + splitArray[2];
                          for (var i2 = 0; i2 < rootnode.children[i1].children.length; i2++) {
                              if (rootnode.children[i1].children[i2].name == nodeValue) {
                                  var foundFlag = false;
                                  nodeValue = nodeValue + "." + splitArray[3];
                                  //search if existing level3 node matches
                                  for (var i3 = 0; i3 < rootnode.children[i1].children[i2].children.length; i3++) {
                                      if (rootnode.children[i1].children[i2].children[i3].name == nodeValue) {
                                          foundFlag = true;
                                      }
                                  }
                                  //if not found, store new level3 node
                                  if (!foundFlag) {
                                      if (arrayLength == 4) {
                                          console.log("HERE3");
                                          rootnode.children[i1].children[i2].children.push({name:nodeValue, children:[],
                                              size:indicatorValue, description:indicatorDescription});
                                      } else {
                                          rootnode.children[i1].children[i2].children.push({name:nodeValue, children:[], size:0});
                                      }
                                  }
                              }
                          }
                      }
                  }
              }
              //Store Level4 eg A1.B2.C1.D1.E1
              if (arrayLength > 4) {
                  // find correct level1 node
                  for (var i1 = 0; i1 < rootnode.children.length; i1++) {
                      var nodeValue = splitArray[0] + "." + splitArray[1];
                      if (rootnode.children[i1].name == nodeValue) {
                          // find correct level2 node
                          nodeValue = nodeValue + "." + splitArray[2];
                          for (var i2 = 0; i2 < rootnode.children[i1].children.length; i2++) {
                              if (rootnode.children[i1].children[i2].name == nodeValue) {
                                  // find correct level3 node
                                  nodeValue = nodeValue + "." + splitArray[3];
                                  for (var i3 = 0; i3 < rootnode.children[i1].children[i2].children.length; i3++) {
                                      if (rootnode.children[i1].children[i2].children[i3].name == nodeValue) {
                                          var foundFlag = false;
                                          nodeValue = nodeValue + "." + splitArray[4];
                                          //search if existing level4 node matches
                                          for (var i4 = 0; i4 < rootnode.children[i1].children[i2].children[i3].children.length; i4++) {
                                              if (rootnode.children[i1].children[i2].children[i3].children[i4].name == nodeValue) {
                                                  foundFlag = true;
                                              }
                                          }
                                          //if not found, store new level4 node
                                          if (!foundFlag) {
                                              if (arrayLength == 5) {
                                                  console.log("HERE4");
                                                  rootnode.children[i1].children[i2].children[i3].children.push({name:nodeValue,
                                                      children:[], size:indicatorValue, description:indicatorDescription});
                                              } else {
                                                  rootnode.children[i1].children[i2].children[i3].children.push({name:nodeValue, children:[], size:0});
                                              }
                                          }
                                      }
                                  }
                              }
                          }
                      }
                  }
              }
          }


          /* ********************************************************************************************* */
//           This method does the actual rendering of the NvD3 graph
//           It uses data constructed from method "extractGraphFormatDataFromElasticResponseArray"
          /* ********************************************************************************************* */
          function plotSunburstGraph(chartData) {
              console.log("in plotChart()");
              var chart;

              nv.addGraph(function() {
                  chart = nv.models.sunburstChart();

                  chart.color(d3.scale.category20c());

                  chart.mode("count");
                  chart.duration(750);
                  chart.height(600);
                  d3.select("#sunburstChart1")
                          .datum(chartData)
                          .call(chart);

                  // Following handles Mouse Click event on the graph nodes
                  // It then updates screen with element name, description, and child node information
                  d3.selectAll(".arc-container")
                          .on("click", function(d) {
                      console.log("d.name=" + d.name);
                      console.log("d.description=" + d.description);

                      document.getElementById("selectedElement").innerHTML = d.name;
                      document.getElementById("selectedDescription").innerHTML = d.description;
                      window.session.selectedGraphIndicator = d.name;
                      refreshTreeGraphSelectedNode(d.name)
//                      if (d.children != null) {
//                          for (var i = 0; i < d.children.length; i++) {
//                              var obj = d.children[i].name;
//                              console.log("child" + i + "=" + obj);
//                          }
//                      }
                  })

                  nv.utils.windowResize(chart.update);

                  return chart;
              });
          }

          function refreshTreeGraphSelectedNode(selectedGraphIndicator) {

              var treeChartElement = document.getElementById("treeChart1");

              var svg_tags2 = document.getElementById("treeChart1").getElementsByTagName("text");
              console.log("TESTING::x.length=" + svg_tags2.length);
              for (var i = 0; i < svg_tags2.length; i++) {
                  var textElement = svg_tags2[i];
                  if (textElement.innerHTML == selectedGraphIndicator) {
                      textElement.setAttribute("class", "treeNodeSelected");
                      console.log("changed" + textElement);
                  } else {
                      textElement.setAttribute("class", "treeNodeNotSelected");
                  }

              }

          }

          function plotTreeGraph(treeData) {
              //JSON object with the data
              var selectedGraphIndicator = window.session.selectedGraphIndicator;
              console.log("selectedGraphIndicator=" + selectedGraphIndicator);

              //NOTE: this is required as node "svg:svg" is always appended to node #viz
              $("#treeChart1").empty();

              //create SVG Canvas
              var myTreeChart = d3.select("#treeChart1").append("svg:svg")
                      .attr("width", 1400)
                      .attr("height", 800)
                      .append("svg:g")
                      .attr("transform", "translate(80, 0)"); // shift everything to the right
              //create Tree Canvas
              var tree = d3.layout.tree()
                      .size([600,400]);

              tree.separation(function separation(a, b) {
                  return a.parent == b.parent ? 1 : 1.5;
              });

              var diagonal = d3.svg.diagonal()
                      .projection(function(d) {
                  return [d.y, d.x];
              });

              var nodes = tree.nodes(treeData);
              var links = tree.links(nodes);

              //NOTE : below determines the distance between horizontal nodes
              nodes.forEach(function(d) {
                  d.y = d.depth * 180;
              });

              console.log("Debug treeData" + treeData)
              console.log("Debug nodes" + nodes)
              console.log("Debug links" + links)

              var link = myTreeChart.selectAll("pathlink")
                      .data(links)
                      .enter().append("svg:path")
                      .attr("class", "link")
                      .attr("d", diagonal)

              //TESTING
              for (var i = 0; i < links.length; i++) {
                  var linkElement = links[i];
                  console.log("links-" + i + "=" + linkElement.source.name);
              }
              var node = myTreeChart.selectAll("g.node")
                      .data(nodes)
                      .enter().append("svg:g")

                      .attr("transform", function(d) {
                  return "translate(" + d.y + "," + d.x + ")";
              })

              // Style dots at nodes

              node.append("svg:circle")
                      .attr("myTag", "NodeCircles")
                      .style("fill", function(d) {
                  // use this option to style later - currently return same style for all nodes
                  return d.children ? "#000" : "#000"
              })
                      .attr("r", function(d) {
                  // use this option to size later - currently return same size for all nodes
                  return d.children ? 15 : 15
              })
                      .on("click", function(d) {
                  //console.log("CLICKED HERE" + d.name)
                  window.location = "/indicator?indicators="+d.name;
              });
              ;

              // below is for node text
              // below determines left/right of node
              node.append("svg:text")
                      .attr("dx", function(d) {
                  return d.children ? -8 : 8;
              })
                      .attr("dy", "20")
                      .attr("text-anchor", function(d) {
                  return d.children ? "end" : "start";
              })
                      .attr("highlight", true)
                  // below determines actual text for node
                      .text(
                           function(d) {
                               return d.name;
                               // use this option to style later - currently return same text for all nodes
                                // return d.children ? d.name : d.name + "--" + d.description;

                           }
                      )
          }

          /* ====================================================================================== */

          return {
              refreshScreen : refreshScreen,
              setupHierarchyIndicators : setupHierarchyIndicators
          }
      }

        )
        ;