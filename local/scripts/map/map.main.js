require(["scripts/map/map.elastic"], function (elastic) {
  elastic.fetchIndicators();
  elastic.fetchProjections();
  elastic.update(window.session.indicators, window.session.projection, window.session.fromYear, window.session.toYear);
    
  $(document).ready(function() {
    $("#indicatorSelect").change(function() {
      //-------- Get the variables that were submitted
      var indicatorsSelected = $('#indicatorSelect').val();

      // ------------ Update the session on the server side
      var formData = 'indicators=' + indicatorsSelected;

      $.post(  
        '/updatesession',  
        formData,  
        function(responseText){
        },  
        "html"  
      );
        
      //-------- Update the session on the client side
      window.session.indicators = indicatorsSelected;

      elastic.update();
      })
    })

    $("#labelsOn").change(function() {
        window.session.mapLabelsOnOff = true;
        elastic.update();
    })

    $("#labelsOff").change(function() {
        window.session.mapLabelsOnOff = false;
        elastic.update();
    })

    $("#projectionSelect").change(function() {
      //-------- Get the variables that were submitted
      var projectionSelected = $('#projectionSelect').val();
      // ------------ Update the session on the server side
      var formData = 'projection=' + projectionSelected;

      $.post(  
        '/updatesession',  
        formData,  
        function(responseText){
        },  
        "html"  
      );
        
      window.session.projection = projectionSelected;

      elastic.update();
    })

    $("#mainForm").submit(function(event){
        var yearsSelected = $("#timeslider>input").slider('getValue');
        var fromYear = yearsSelected[0];
        var toYear = yearsSelected[1];

        var formData = 'fromYear=' + fromYear + '&toYear=' + toYear;

        $.post('/updatesession', formData, function(responseText){}, "html");

        window.session.fromYear = fromYear;
        window.session.toYear = toYear;

        elastic.update();
    
        event.preventDefault();
  })
});