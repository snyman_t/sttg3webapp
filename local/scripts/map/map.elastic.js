define([
  'datamaps/js/topojson',
  'd3/d3.min',
  'elasticsearch/elasticsearch',
  'datamaps/js/datamaps.world'], 
  function (topojson, d3, elasticsearch, datamaps) {
    "use strict";
    var client = new elasticsearch.Client({ host: window.globalVariables.elasticsearch_URL});

    function fetchProjections(){ 
      var projections = new Array();

      projections.push({value: "world", name: "World"});
      projections.push({value: "africa", name: "Africa"});
      projections.push({value: "asia", name: "Asia"});
      projections.push({value: "australia", name: "Australia"});
      projections.push({value: "europe", name: "Europe"});
      projections.push({value: "northAmerica", name: "North America"});
      projections.push({value: "southAmerica", name: "South America"});
      projections.push({value: "southeastAsia", name: "South East Asia"});

      var projectionsSelected = window.session.indicator;
      if (projectionsSelected == null || projectionsSelected == undefined)
      {
        projectionsSelected = "world";
        window.session.projection  = projectionsSelected;
      }
      for(var i = 0; i < projections.length; i++){
          var selected = '';
          if(projections[i].value == projectionsSelected){
            selected = 'selected';
          }

          $('#projectionSelect').append('<option value="'+ projections[i].value + '" ' + selected +' >' + projections[i].name +'</option>');
      }

      $('#projectionSelect').multiselect();
    }

    function fetchIndicators(){
      client.search(
      {
        index: window.globalVariables.elasticsearch_index,
        size: 10,
        body: {
          "aggs" : {
              "indicators" : { "terms" : { "size" : 100, "field" : "Indicator Code" } }
          }
        }
      }).then(function (response) { 
        var indicators = new Array();
        for(var i = 0; i < response.aggregations.indicators.buckets.length; i++){
          indicators.push((response.aggregations.indicators.buckets[i].key).toUpperCase());
        }

        indicators.sort();
        var indicatorsSelected = window.session.indicators.split(',');

        for(var i = 0; i < indicators.length; i++){
            
            var selected = '';
            if(indicatorsSelected.indexOf(indicators[i]) != -1){
              selected = 'selected';
            }

            $('#indicatorSelect').append('<option value="'+indicators[i]+ '" '+selected+' >'+indicators[i]+'</option>');
        }

        $('#indicatorSelect').multiselect();
      });    
    }

    function update()
    {
      var indicator = window.session.indicators;
      var projection = window.session.projection;
      var fromYear = window.session.fromYear;
      var toYear = window.session.toYear;
      var mapLabelsOnOff = window.session.mapLabelsOnOff;

     // console.log(indicator, projection, fromYear, toYear, mapLabelsOnOff);

      client.search({
        index: window.globalVariables.elasticsearch_index,
        body: {
          size : 10000,
          query: {
            bool: {
              must: 
              [
                {match: {"Indicator Code": indicator }},
                {range: { "Year": { gte: fromYear, lte: toYear } }}
              ],
              filter: {exists: { field: "Value" }}
            }
          },
          aggs: {
            "country_grouping": {
              terms: {
                field: "Country Code",
                "size": 10000
              },
              aggs: { 
                "avg_value": {
                  avg: {
                    field: "Value"
                  }
                },
                "max_value": {
                  max: {
                    field: "Value"
                  }
                },
                "min_value": {
                  min: {
                    field: "Value"
                  }
                }
              }
            }
          }
        }
      }).then(function (resp) {
        renderMap(resp, projection, mapLabelsOnOff);
      });
    }

    function getProjectionCoordinates(scope)
    {
      var dataset ={};
      dataset["africa"] = {x: 23, y:-12, z: 400};
      dataset["asia"] = {x: 90, y:20, z: 400};
      dataset["southeastAsia"] = {x: 130, y:20, z: 600};
      dataset["australia"] = {x: 150, y:-50, z: 400};
      dataset["northAmerica"] = {x: -90, y:65, z: 300};
      dataset["southAmerica"] = {x: -65, y:-30, z: 400};
      dataset["europe"] = {x: 10, y:30, z: 300};

      return dataset[scope];
    }

    function renderMap(resp, scope, mapLabelsOnOff)
    {
      try {

        if (resp == null)
          return;

        var results = resp.aggregations.country_grouping.buckets.map(function(obj) {
                        return { 0: obj.key.toUpperCase(), 1: obj.avg_value.value}; 
                      });
        document.getElementById("map_header").innerHTML = (resp.hits.hits[0]._source["Indicator Name"]);
        // We need to colorize every country based on numberOfWhatever"
        // colors should be uniq for every value.
        // For this purpose we create palette(using min/max series-value)
        var onlyValues = results.map(function(obj){ return obj[1]; }).sort((a, b) => a - b);
        var minValue = Math.min.apply(null, onlyValues);
        var maxValue = Math.max.apply(null, onlyValues);
        //var median = (onlyValues[(onlyValues.length - 1) >> 1] + onlyValues[onlyValues.length >> 1]) / 2;
        onlyValues.sort();
        // create color palette function
        // color can be whatever you wish
        var nonZeroMinValue = 0;
        for (var i = 0.0; i<=onlyValues.length; i++)
        {
          if (onlyValues[i] != 0)
          {
            nonZeroMinValue=onlyValues[i];
            break;
          }
        }

        var domain = [];
        var next = nonZeroMinValue;
        var step = (maxValue - nonZeroMinValue) /9;
        for (var i = nonZeroMinValue; i < maxValue; i += step)
        {
          //console.log(next, i);
          domain.push(i);
          next = i;
        }

        domain.sort();
        console.log(domain.sort());

        var heatmapColour = d3.scale.quantize()
                  .domain(domain)
                  .range(colorbrewer.Blues[9]);

        // fill dataset in appropriate format
        var dataset = {};
        var legend = [];
        results.forEach(function(item){
            var iso = item[0];
            var value = item[1];
            var color = heatmapColour(value);
            dataset[iso] = { numberOfThings: value, fillColor: color };

            legend[color] = (legend[color] == null) ? { min: value, max: value}
                                          : { min: Math.min(legend[color].min, value), max: Math.max(legend[color].max, value)};

            //console.log(color, value);
        });

        document.getElementById("map").innerHTML = "";
        var map = new Datamap({
            element: document.getElementById('map'),
            scope: "world",  
            // Zoom in on Africa
            setProjection: function(element) { 
              var item = getProjectionCoordinates(scope);
              if (item != null)
              {
                var projection = d3.geo.mercator()
                    .center([item.x, item.y])
                    .rotate([4.4, 0])
                    .scale(item.z)
                    .translate([element.offsetWidth / 2, element.offsetHeight / 2]);
                var path = d3.geo.path()
                    .projection(projection);

                return {path: path, projection: projection};
              }
              else
              {
                var projection = d3.geo.mercator()
                  .center([10,45]) 
                  .rotate([4.4, 0])
                  .scale(200)
                  .translate([element.offsetWidth / 2, element.offsetHeight / 2]);
                var path = d3.geo.path()
                  .projection(projection);

                return {path: path, projection: projection};
              }
            },
            // countries don't listed in dataset will be painted with this color
            fills://{ defaultFill: '#F5F5F5' },
            {
              defaultFill: '#ffffff'
            },
            data: dataset,
            geographyConfig: {
                borderColor: '#DEDEDE',
                highlightBorderWidth: 2,
                // don't change color on mouse hover
                highlightFillColor: function(geo) {
                    return geo['fillColor'] || '#ffffff';
                },
                // only change border
                highlightBorderColor: '#fec44f',
                // show desired information in tooltip
                popupTemplate: function(geo, data) {
                    // don't show tooltip if country don't present in dataset
                    if (!data) { return ; }
                    // tooltip content
                    return ['<div class="hoverinfo">',
                        '<strong>', geo.properties.name, '</strong>',
                        '<br>Value: <strong>', data.numberOfThings, '</strong>',
                        '</div>'].join('');
                }
            }
        });

        if (mapLabelsOnOff)
        {
          map.labels();
        }
        map.legend(
          {
            fills : legend
          });
      } catch (error) {
          console.log(error);
      }
    }
    // This product includes color specifications and designs developed by Cynthia Brewer (http://colorbrewer.org/).
    // JavaScript specs as packaged in the D3 library (d3js.org). Please see license at http://colorbrewer.org/export/LICENSE.txt
    var colorbrewer = 
    { 
      YlGnBu: {
        3: ["#edf8b1","#7fcdbb","#2c7fb8"],
        4: ["#ffffcc","#a1dab4","#41b6c4","#225ea8"],
        5: ["#ffffcc","#a1dab4","#41b6c4","#2c7fb8","#253494"],
        6: ["#ffffcc","#c7e9b4","#7fcdbb","#41b6c4","#2c7fb8","#253494"],
        7: ["#ffffcc","#c7e9b4","#7fcdbb","#41b6c4","#1d91c0","#225ea8","#0c2c84"],
        8: ["#ffffd9","#edf8b1","#c7e9b4","#7fcdbb","#41b6c4","#1d91c0","#225ea8","#0c2c84"],
        9: ["#ffffd9","#edf8b1","#c7e9b4","#7fcdbb","#41b6c4","#1d91c0","#225ea8","#253494","#081d58"]
      },
      Reds: {
        3: ["#fee0d2","#fc9272","#de2d26"],
        4: ["#fee5d9","#fcae91","#fb6a4a","#cb181d"],
        5: ["#fee5d9","#fcae91","#fb6a4a","#de2d26","#a50f15"],
        6: ["#fee5d9","#fcbba1","#fc9272","#fb6a4a","#de2d26","#a50f15"],
        7: ["#fee5d9","#fcbba1","#fc9272","#fb6a4a","#ef3b2c","#cb181d","#99000d"],
        8: ["#fff5f0","#fee0d2","#fcbba1","#fc9272","#fb6a4a","#ef3b2c","#cb181d","#99000d"],
        9: ["#fff5f0","#fee0d2","#fcbba1","#fc9272","#fb6a4a","#ef3b2c","#cb181d","#a50f15","#67000d"]
      },
      Blues: {
        3: ["#deebf7","#9ecae1","#3182bd"],
        4: ["#eff3ff","#bdd7e7","#6baed6","#2171b5"],
        5: ["#eff3ff","#bdd7e7","#6baed6","#3182bd","#08519c"],
        6: ["#eff3ff","#c6dbef","#9ecae1","#6baed6","#3182bd","#08519c"],
        7: ["#eff3ff","#c6dbef","#9ecae1","#6baed6","#4292c6","#2171b5","#084594"],
        8: ["#f7fbff","#deebf7","#c6dbef","#9ecae1","#6baed6","#4292c6","#2171b5","#084594"],
        9: ["#f7fbff","#deebf7","#c6dbef","#9ecae1","#6baed6","#4292c6","#2171b5","#08519c","#08306b"]
        },
      Greens: {
        3: ["#e5f5e0","#a1d99b","#31a354"],
        4: ["#edf8e9","#bae4b3","#74c476","#238b45"],
        5: ["#edf8e9","#bae4b3","#74c476","#31a354","#006d2c"],
        6: ["#edf8e9","#c7e9c0","#a1d99b","#74c476","#31a354","#006d2c"],
        7: ["#edf8e9","#c7e9c0","#a1d99b","#74c476","#41ab5d","#238b45","#005a32"],
        8: ["#f7fcf5","#e5f5e0","#c7e9c0","#a1d99b","#74c476","#41ab5d","#238b45","#005a32"],
        9: ["#f7fcf5","#e5f5e0","#c7e9c0","#a1d99b","#74c476","#41ab5d","#238b45","#006d2c","#00441b"]
      }
    };

    return {
      update : update,
      fetchIndicators : fetchIndicators,
      fetchProjections : fetchProjections
    }
});